# Majoo Test Case

![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/rzrizki/testcase/main)

```
[✓] Flutter (Channel stable, 2.8.1, on macOS 12.0.1 21A559 darwin-arm, locale en-US)
    • Flutter version 2.8.1 at /Users/rizkimaulana/development/flutter
    • Upstream repository https://github.com/flutter/flutter.git
    • Framework revision 77d935af4d (6 weeks ago), 2021-12-16 08:37:33 -0800
    • Engine revision 890a5fca2e
    • Dart version 2.15.1
```


- Implementing clean architecture
- Migrated to null safety
- State management using Bloc
- Network checking
- Local storage using Sqflite and shared preferences
- Dependency injection using get_it
- Continuous integration Using bitbucket pipelines
- Static dart code analysis using flutter_lints
- etc..