import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:majootestcase/core/network/network_info.dart';
import 'package:majootestcase/core/utils/constant.dart';
import 'package:majootestcase/core/utils/shared_preferences_manager.dart';
import 'package:majootestcase/data/data_sources/db/database_helper.dart';
import 'package:majootestcase/data/data_sources/local/user_local_data_soure.dart';
import 'package:majootestcase/data/data_sources/remote/movie_remote_data_source.dart';
import 'package:majootestcase/data/repositories/movie_repository_impl.dart';
import 'package:majootestcase/data/repositories/user_repository_impl.dart';
import 'package:majootestcase/domain/repositories/movie_repository.dart';
import 'package:majootestcase/domain/repositories/user_repository.dart';
import 'package:majootestcase/domain/use_cases/get_movie_list.dart';
import 'package:majootestcase/domain/use_cases/login.dart';
import 'package:majootestcase/domain/use_cases/register.dart';
import 'package:majootestcase/presentation/bloc/auth/auth_bloc.dart';
import 'package:majootestcase/presentation/bloc/home/home_bloc.dart';
import 'package:majootestcase/presentation/bloc/register/register_bloc.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init() async {
  /**
   * ! Features
   */
  // Bloc
  sl.registerFactory(
    () => HomeBloc(
      getMovieList: sl(),
    ),
  );
  sl.registerFactory(
    () => AuthBloc(
      login: sl(),
      sharedPreferencesManager: sl(),
    ),
  );
  sl.registerFactory(
    () => RegisterBloc(
      register: sl(),
      sharedPreferencesManager: sl(),
    ),
  );

  // Use case
  sl.registerLazySingleton(() => GetMovieList(movieRepository: sl()));
  sl.registerLazySingleton(() => Login(userRepository: sl()));
  sl.registerLazySingleton(() => Register(userRepository: sl()));

  // Repository
  sl.registerLazySingleton<MovieRepository>(
    () => MovieRepositoryImpl(
      movieRemoteDataSource: sl(),
      networkInfo: sl(),
    ),
  );
  sl.registerLazySingleton<UserRepository>(
    () => UserRepositoryImpl(
      userLocalDataSource: sl(),
    ),
  );

  // Data source
  sl.registerLazySingleton<MovieRemoteDataSource>(
    () => MovieRemoteDataSourceImpl(
      dio: sl(),
    ),
  );
  sl.registerLazySingleton<UserLocalDataSource>(
    () => UserLocalDataSourceImpl(
      databaseHelper: sl(),
    ),
  );

  /**
   * ! Core
   */
  sl.registerLazySingleton<DatabaseHelper>(() => DatabaseHelper());
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  /**
   * ! External
   */
  final sharedPreferences = await SharedPreferences.getInstance();
  final sharedPreferencesManager = SharedPreferencesManager.getInstance(
    sharedPreferences,
  );
  sl.registerLazySingleton(() => sharedPreferencesManager);

  sl.registerLazySingleton(() {
    var options = BaseOptions(
      baseUrl: ApiConfig.baseUrl,
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": ApiConfig.apiKey,
        "x-rapidapi-host": ApiConfig.host,
      },
    );

    final dio = Dio(options);
    dio.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90,
    ));
    return dio;
  });

  sl.registerLazySingleton(() => Connectivity());
}
