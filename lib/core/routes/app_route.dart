import 'package:flutter/material.dart';
import 'package:majootestcase/presentation/pages/detail/detail_movie_page.dart';
import 'package:majootestcase/presentation/pages/home/home_page.dart';
import 'package:majootestcase/presentation/pages/login/login_page.dart';
import 'package:majootestcase/presentation/pages/register/register_page.dart';

class AppRoute {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case LoginPage.routeName:
        return MaterialPageRoute(builder: (_) => const LoginPage());
      case RegisterPage.routeName:
        return MaterialPageRoute(builder: (_) => const RegisterPage());
      case HomePage.routeName:
        return MaterialPageRoute(builder: (_) => const HomePage());
      case DetailMoviePage.routeName:
        final arguments = settings.arguments as Map<String, dynamic>;
        final movie = arguments['movie'];
        return MaterialPageRoute(builder: (_) => DetailMoviePage(movie: movie));
      default:
        return MaterialPageRoute(
          builder: (_) => const Scaffold(
            backgroundColor: Colors.white,
            body: Center(
              child: Text('Page Not Found'),
            ),
          ),
        );
    }
  }
}
