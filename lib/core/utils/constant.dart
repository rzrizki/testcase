class PreferenceKey {
  static const isLoggedIn = "isLoggedIn";
  static const userName = "userName";
  static const email = "email";
}

class ApiConfig {
  static const baseUrl = "https://imdb8.p.rapidapi.com/";
  static const apiKey = "270fa3c13fmshb43ad54eac3cdb5p142010jsne1f8bac6f686";
  static const host = "imdb8.p.rapidapi.com";
}

class DatabaseTable {
  static const user = "user";
  static const columnEmail = "email";
  static const columnPassword = "password";
  static const columnUserName = "userName";
}

class ConstantMessage {
  static const unknownError = "Unknown Error";
  static const connectionError = "Connection Error";
  static const authSuccess = "Login Berhasil";
  static const insertUserSuccess = "Register Berhasil";
  static const userAlreadyExisted = "Email ini sudah pernah terdaftar";
  static const loginFailed = "Login gagal , periksa kembali inputan anda";
  static const emailInvalid = "Masukkan e-mail yang valid";
  static const fieldRequired = "Field tidak boleh kosong";
}
