import 'package:equatable/equatable.dart';
import 'package:majootestcase/domain/entities/api_data_failure/api_data_failure.dart';

abstract class Failure extends Equatable {}

class ServerFailure extends Failure {
  final ApiDataFailure apiDataFailure;

  ServerFailure(this.apiDataFailure);

  @override
  List<Object> get props => [apiDataFailure];
}

class DatabaseFailure extends Failure {
  final String errorMessage;

  DatabaseFailure(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}

class ConnectionFailure extends Failure {
  final String errorMessage;

  ConnectionFailure(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}

class ParsingFailure extends Failure {
  final String errorMessage;

  ParsingFailure(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}
