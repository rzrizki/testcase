import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/core/routes/app_route.dart';
import 'package:majootestcase/injection_container.dart';
import 'package:majootestcase/presentation/bloc/auth/auth_bloc.dart';
import 'package:majootestcase/presentation/pages/home/home_page.dart';
import 'package:majootestcase/presentation/pages/login/login_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.green,
        ),
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      title: 'Majoo Movie App',
      home: BlocProvider(
        create: (context) => sl<AuthBloc>()..add(LoadAuthStatusEvent()),
        child: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            if (state is AuthLoggedInState) {
              return const HomePage();
            } else if (state is AuthLoginState) {
              return const LoginPage();
            }
            return Container();
          },
        ),
      ),
      onGenerateRoute: AppRoute.generateRoute,
    );
  }
}
