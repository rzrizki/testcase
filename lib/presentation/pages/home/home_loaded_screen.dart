import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/domain/entities/movie/movie.dart';
import 'package:majootestcase/presentation/pages/detail/detail_movie_page.dart';
import 'package:majootestcase/presentation/widgets/empty_screen.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Movie> movieList;

  const HomeBlocLoadedScreen({Key? key, this.movieList = const []})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (movieList.isEmpty) {
      return const EmptyScreen(
        message: "Result not found",
      );
    }
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: movieList.length,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return _movieItemWidget(movieList[index], context);
      },
    );
  }

  Widget _movieItemWidget(Movie movie, context) {
    final title = movie.title ?? '';
    final imageUrl = movie.image?.imageUrl ?? '';
    return imageUrl.isNotEmpty
        ? Container(
            margin: const EdgeInsets.only(bottom: 8),
            child: GestureDetector(
              onTap: () {
                Navigator.pushNamed(
                  context,
                  DetailMoviePage.routeName,
                  arguments: {'movie': movie},
                );
              },
              child: Card(
                elevation: 4,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16.0))),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.only(
                          top: 16,
                          right: 16,
                          left: 16,
                        ),
                        child: CachedNetworkImage(
                          imageUrl: imageUrl,
                          errorWidget: (context, url, error) =>
                              const Icon(Icons.error),
                          width: double.infinity,
                        ),
                      ),
                    ),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 20,
                        ),
                        child: Text(
                          title,
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        : Container();
  }
}
