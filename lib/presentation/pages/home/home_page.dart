import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/core/utils/constant.dart';
import 'package:majootestcase/injection_container.dart';
import 'package:majootestcase/presentation/bloc/auth/auth_bloc.dart';
import 'package:majootestcase/presentation/bloc/home/home_bloc.dart';
import 'package:majootestcase/presentation/pages/login/login_page.dart';
import 'package:majootestcase/presentation/widgets/empty_screen.dart';
import 'package:majootestcase/presentation/widgets/error_screen.dart';
import 'package:majootestcase/presentation/widgets/loading.dart';
import 'package:majootestcase/presentation/widgets/text_field_search.dart';
import 'home_loaded_screen.dart';

class HomePage extends StatefulWidget {
  static const routeName = 'home';

  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _homeBloc = sl<HomeBloc>();
  final _authBloc = sl<AuthBloc>();
  String _keyword = '';

  @override
  void initState() {
    _searchMovie();
    super.initState();
  }

  void _searchMovie() => _homeBloc.add(LoadMovieHomeEvent(query: _keyword));

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => _homeBloc,
        ),
        BlocProvider(
          create: (context) => _authBloc,
        ),
      ],
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Majoo Movie App'),
          actions: [
            _buildLogoutButton(),
          ],
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: [
                const SizedBox(height: 4),
                _buildSearchField(),
                const SizedBox(height: 16),
                Expanded(
                  child: BlocBuilder<HomeBloc, HomeState>(
                    builder: (context, state) {
                      if (state is HomeLoadedState) {
                        return HomeBlocLoadedScreen(
                            movieList: state.movieList ?? []);
                      } else if (state is HomeLoadingState) {
                        return const LoadingIndicator();
                      } else if (state is HomeInitialState) {
                        return Container();
                      } else if (state is HomeErrorState) {
                        return ErrorScreen(
                          message: state.errorMessage ??
                              ConstantMessage.unknownError,
                          retry: () => _searchMovie(),
                        );
                      } else if (state is HomeEmptyState) {
                        return const EmptyScreen(
                          message: "Empty result, try search something...",
                        );
                      }
                      return Container();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildLogoutButton() {
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is AuthLoggedOutState) {
          Navigator.pushNamedAndRemoveUntil(
            context,
            LoginPage.routeName,
            (route) => false,
          );
        }
        if (state is AuthErrorState) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(
                state.errorMessage ?? ConstantMessage.unknownError,
              ),
            ),
          );
        }
      },
      child: IconButton(
        onPressed: () async {
          final confirmLogout = await showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: const Text('Konfirmasi Logout'),
                content: const Text('Yakin ingin keluar dari akun?'),
                actions: [
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Kembali'),
                  ),
                  TextButton(
                    onPressed: () => Navigator.pop(context, true),
                    child: const Text('Konfirmasi'),
                  ),
                ],
              );
            },
          );
          if (confirmLogout is bool && confirmLogout) {
            _authBloc.add(SubmitLogOutEvent());
          }
        },
        icon: const Icon(Icons.logout),
      ),
    );
  }

  Widget _buildSearchField() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(999),
        color: Colors.black12,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 4,
      ),
      child: Row(
        children: [
          const Icon(
            Icons.search,
            color: Colors.grey,
          ),
          const SizedBox(width: 8),
          Expanded(
            child: TextFieldSearch(
              (keyword) {
                _keyword = keyword;
                if (_keyword.isNotEmpty) {
                  _searchMovie();
                }
              },
              decoration: const InputDecoration(
                isDense: true,
                hintText: "Search...",
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
