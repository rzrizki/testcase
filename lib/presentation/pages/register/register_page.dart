import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/core/utils/constant.dart';
import 'package:majootestcase/domain/entities/user/user.dart';
import 'package:majootestcase/injection_container.dart';
import 'package:majootestcase/presentation/bloc/register/register_bloc.dart';
import 'package:majootestcase/presentation/pages/home/home_page.dart';
import 'package:majootestcase/presentation/widgets/custom_button.dart';

class RegisterPage extends StatefulWidget {
  static const routeName = 'register';

  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _userNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _registerBloc = sl<RegisterBloc>();

  bool _isObscurePassword = true;

  void _submitRegister() async {
    FocusManager.instance.primaryFocus?.unfocus();
    final userName = _userNameController.text;
    final email = _emailController.text;
    final password = _passwordController.text;
    if (_formKey.currentState!.validate()) {
      User user = User(
        userName: userName,
        email: email,
        password: password,
      );
      _registerBloc.add(SubmitRegisterEvent(user: user));
    }
  }

  void _showSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(message)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _registerBloc,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: BlocListener<RegisterBloc, RegisterState>(
          listener: (context, state) async {
            if (state is RegisterSuccessState) {
              _showSnackBar(state.message);
              await Navigator.pushNamedAndRemoveUntil(
                context,
                HomePage.routeName,
                (route) => false,
              );
            } else if (state is RegisterErrorState) {
              _showSnackBar(state.errorMessage ?? ConstantMessage.unknownError);
            }
          },
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text(
                    'Register',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      // color: colorBlue,
                    ),
                  ),
                  const SizedBox(height: 8),
                  const Text(
                    'Buat akun baru kamu',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  _buildForm(),
                  const SizedBox(
                    height: 24,
                  ),
                  CustomButton(
                    text: 'Register',
                    onPressed: _submitRegister,
                    height: 100,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  _buildRegisterButton(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: _userNameController,
            keyboardType: TextInputType.text,
            decoration: const InputDecoration(
              label: Text('Username'),
              hintText: 'Majoo super apps',
            ),
            validator: (val) {
              if (val != null) {
                if (val.isEmpty) {
                  return ConstantMessage.fieldRequired;
                }
              }
            },
          ),
          const SizedBox(
            height: 16,
          ),
          TextFormField(
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            decoration: const InputDecoration(
              label: Text('Email'),
              hintText: 'Example@123.com',
            ),
            validator: (val) {
              final pattern = RegExp(
                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+",
              );
              if (val != null) {
                if (val.isEmpty) {
                  return ConstantMessage.fieldRequired;
                }
                return pattern.hasMatch(val)
                    ? null
                    : ConstantMessage.emailInvalid;
              }
            },
          ),
          const SizedBox(
            height: 16,
          ),
          TextFormField(
            decoration: InputDecoration(
              label: const Text('Password'),
              hintText: 'password',
              suffixIcon: IconButton(
                icon: Icon(
                  _isObscurePassword
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                ),
                onPressed: () {
                  setState(() {
                    _isObscurePassword = !_isObscurePassword;
                  });
                },
              ),
            ),
            keyboardType: TextInputType.visiblePassword,
            controller: _passwordController,
            obscureText: _isObscurePassword,
            validator: (val) {
              if (val != null) {
                if (val.isEmpty) {
                  return ConstantMessage.fieldRequired;
                }
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildRegisterButton() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () => Navigator.pop(context),
        child: RichText(
          text: const TextSpan(
            text: 'Sudah punya akun? ',
            style: TextStyle(color: Colors.black45),
            children: [
              TextSpan(
                text: 'Masuk',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
