import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/core/utils/constant.dart';
import 'package:majootestcase/domain/entities/user/user.dart';
import 'package:majootestcase/injection_container.dart';
import 'package:majootestcase/presentation/bloc/auth/auth_bloc.dart';
import 'package:majootestcase/presentation/pages/home/home_page.dart';
import 'package:majootestcase/presentation/pages/register/register_page.dart';
import 'package:majootestcase/presentation/widgets/custom_button.dart';

class LoginPage extends StatefulWidget {
  static const routeName = 'login';

  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _authBloc = sl<AuthBloc>();

  bool _isObscurePassword = true;

  void _submitLogin() async {
    FocusManager.instance.primaryFocus?.unfocus();
    final email = _emailController.text;
    final password = _passwordController.text;
    if (_formKey.currentState!.validate()) {
      User user = User(
        email: email,
        password: password,
      );
      _authBloc.add(SubmitLoginEvent(user: user));
    }
  }

  void _showSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(message)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _authBloc,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: BlocListener<AuthBloc, AuthState>(
          listener: (context, state) async {
            if (state is AuthSuccesState) {
              _showSnackBar(ConstantMessage.authSuccess);
              await Navigator.pushReplacementNamed(context, HomePage.routeName);
            } else if (state is AuthErrorState) {
              _showSnackBar(state.errorMessage ?? ConstantMessage.unknownError);
            }
          },
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text(
                    'Selamat Datang',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      // color: colorBlue,
                    ),
                  ),
                  const SizedBox(height: 8),
                  const Text(
                    'Silahkan login terlebih dahulu',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  _buildForm(),
                  const SizedBox(
                    height: 24,
                  ),
                  CustomButton(
                    text: 'Login',
                    onPressed: _submitLogin,
                    height: 100,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  _buildRegisterButton(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            decoration: const InputDecoration(
              label: Text('Email'),
              hintText: 'Example@123.com',
            ),
            validator: (val) {
              final pattern = RegExp(
                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+",
              );
              if (val != null) {
                if (val.isEmpty) {
                  return ConstantMessage.fieldRequired;
                }
                return pattern.hasMatch(val)
                    ? null
                    : ConstantMessage.emailInvalid;
              }
            },
          ),
          const SizedBox(
            height: 16,
          ),
          TextFormField(
            decoration: InputDecoration(
              label: const Text('Password'),
              hintText: 'password',
              suffixIcon: IconButton(
                icon: Icon(
                  _isObscurePassword
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                ),
                onPressed: () {
                  setState(() {
                    _isObscurePassword = !_isObscurePassword;
                  });
                },
              ),
            ),
            keyboardType: TextInputType.visiblePassword,
            controller: _passwordController,
            obscureText: _isObscurePassword,
            validator: (val) {
              if (val != null) {
                if (val.isEmpty) {
                  return ConstantMessage.fieldRequired;
                }
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildRegisterButton() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () => Navigator.pushNamed(context, RegisterPage.routeName),
        child: RichText(
          text: const TextSpan(
            text: 'Belum punya akun? ',
            style: TextStyle(color: Colors.black45),
            children: [
              TextSpan(
                text: 'Daftar',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
