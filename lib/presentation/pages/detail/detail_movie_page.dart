import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/domain/entities/movie/movie.dart';

class DetailMoviePage extends StatefulWidget {
  static const routeName = 'detail-movie';

  final Movie movie;

  const DetailMoviePage({
    Key? key,
    required this.movie,
  }) : super(key: key);

  @override
  State<DetailMoviePage> createState() => _DetailMoviePageState();
}

class _DetailMoviePageState extends State<DetailMoviePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildDetailContent(),
    );
  }

  Widget _buildDetailContent() {
    return SingleChildScrollView(
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              _buildBackButton(),
              _buildMainImage(),
              _buildTitle(),
              _buildYear(),
              const Divider(color: Colors.black38),
              _buildVideos(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBackButton() {
    return Row(
      children: [
        GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Container(
            padding: const EdgeInsets.only(top: 24.0, bottom: 16.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const Icon(
                  Icons.arrow_back_rounded,
                  size: 20,
                  color: Colors.green,
                ),
                const SizedBox(width: 4.0),
                Text(
                  'Kembali',
                  style: Theme.of(context).textTheme.subtitle1?.copyWith(
                        color: Colors.green,
                      ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildMainImage() {
    final imageUrl = widget.movie.image?.imageUrl ?? '';
    if (imageUrl.isEmpty) {
      return Container();
    }
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: CachedNetworkImage(
        imageUrl: imageUrl,
        errorWidget: (context, url, error) => const Icon(Icons.error),
        height: 240,
      ),
    );
  }

  Widget _buildTitle() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Text(
          widget.movie.title ?? '-',
          style: Theme.of(context).textTheme.headline5,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget _buildYear() {
    final year = '${widget.movie.year ?? widget.movie.yearString ?? ''}';
    if (year.isEmpty) {
      return Container();
    }
    return Center(
      child: Text(
        '($year)',
        style: Theme.of(context).textTheme.subtitle1,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildVideos() {
    final videos = widget.movie.videoList ?? [];

    return Container(
      margin: const EdgeInsets.only(top: 16),
      height: 200,
      child: ListView.builder(
        physics: const BouncingScrollPhysics(),
        itemCount: videos.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          final imageUrl = videos[index].image?.imageUrl ?? '';
          final title = videos[index].title ?? '';
          if (imageUrl.isEmpty) {
            return Container();
          }
          return Container(
            margin: const EdgeInsets.only(right: 16),
            width: 260,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CachedNetworkImage(
                  imageUrl: imageUrl,
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                  height: 160,
                  width: 260,
                  fit: BoxFit.fitWidth,
                ),
                const SizedBox(height: 8),
                Text(
                  title,
                  maxLines: 1,
                  style: Theme.of(context).textTheme.caption,
                  overflow: TextOverflow.ellipsis,
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
