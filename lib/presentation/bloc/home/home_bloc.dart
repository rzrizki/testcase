import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/core/error/failure.dart';
import 'package:majootestcase/core/utils/constant.dart';
import 'package:majootestcase/domain/entities/movie/movie.dart';
import 'package:majootestcase/domain/use_cases/get_movie_list.dart';
import 'package:rxdart/rxdart.dart';

part 'home_state.dart';
part 'home_event.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final GetMovieList getMovieList;

  HomeBloc({
    required this.getMovieList,
  }) : super(HomeInitialState()) {
    on<LoadMovieHomeEvent>(
      _onLoadMovieHomeEvent,
      transformer: (events, mapper) => restartable<LoadMovieHomeEvent>().call(
        events.debounceTime(const Duration(milliseconds: 500)),
        mapper,
      ),
    );
  }

  void _onLoadMovieHomeEvent(
    LoadMovieHomeEvent event,
    Emitter<HomeState> emit,
  ) async {
    emit(HomeLoadingState());

    if (event.query.isEmpty) {
      emit(HomeEmptyState());
      return;
    }

    final resultHome = await getMovieList(
      ParamsGetMovieList(query: event.query),
    );

    emit(
      resultHome.fold(
        (failure) {
          var errorMessage = ConstantMessage.unknownError;
          if (failure is ServerFailure) {
            errorMessage = failure.apiDataFailure.message ?? errorMessage;
          } else if (failure is ConnectionFailure) {
            errorMessage = failure.errorMessage;
          } else if (failure is ParsingFailure) {
            errorMessage = failure.errorMessage;
          }
          return HomeErrorState(errorMessage: errorMessage);
        },
        (response) => HomeLoadedState(movieList: response),
      ),
    );
  }
}
