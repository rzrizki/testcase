part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object?> get props => [];
}

class HomeInitialState extends HomeState {}

class HomeLoadingState extends HomeState {}

class HomeEmptyState extends HomeState {}

class HomeLoadedState extends HomeState {
  final List<Movie>? movieList;

  const HomeLoadedState({this.movieList});

  @override
  List<Object?> get props => [movieList];
}

class HomeErrorState extends HomeState {
  final String? errorMessage;

  const HomeErrorState({
    this.errorMessage,
  });

  @override
  List<Object?> get props => [errorMessage];
}
