part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class LoadMovieHomeEvent extends HomeEvent {
  final String query;

  const LoadMovieHomeEvent({required this.query});

  @override
  List<Object?> get props => [query];
}
