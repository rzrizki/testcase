part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class SubmitRegisterEvent extends RegisterEvent {
  final User user;

  const SubmitRegisterEvent({required this.user});

  @override
  List<Object?> get props => [user];
}
