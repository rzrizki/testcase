import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/core/error/failure.dart';
import 'package:majootestcase/core/utils/constant.dart';
import 'package:majootestcase/core/utils/shared_preferences_manager.dart';
import 'package:majootestcase/domain/entities/user/user.dart';
import 'package:majootestcase/domain/use_cases/register.dart';

part 'register_state.dart';
part 'register_event.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final Register register;
  final SharedPreferencesManager sharedPreferencesManager;

  RegisterBloc({
    required this.register,
    required this.sharedPreferencesManager,
  }) : super(RegisterInitialState()) {
    on<SubmitRegisterEvent>(_onSubmitRegisterEvent, transformer: restartable());
  }

  void _onSubmitRegisterEvent(
    SubmitRegisterEvent event,
    Emitter<RegisterState> emit,
  ) async {
    emit(RegisterLoadingState());

    final resultRegister = await register(
      ParamsRegister(user: event.user),
    );

    emit(
      await resultRegister.fold(
        (failure) {
          var errorMessage = ConstantMessage.unknownError;
          if (failure is DatabaseFailure) {
            errorMessage = failure.errorMessage;
          }
          return RegisterErrorState(errorMessage: errorMessage);
        },
        (response) async {
          await sharedPreferencesManager.putBool(
            PreferenceKey.isLoggedIn,
            true,
          );
          await sharedPreferencesManager.putString(
            PreferenceKey.email,
            event.user.email ?? '',
          );
          await sharedPreferencesManager.putString(
            PreferenceKey.userName,
            event.user.userName ?? '',
          );
          return RegisterSuccessState(message: response);
        },
      ),
    );
  }
}
