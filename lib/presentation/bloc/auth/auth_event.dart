part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class LoadAuthStatusEvent extends AuthEvent {
  @override
  List<Object?> get props => [];
}

class SubmitLogOutEvent extends AuthEvent {
  @override
  List<Object?> get props => [];
}

class SubmitLoginEvent extends AuthEvent {
  final User user;

  const SubmitLoginEvent({required this.user});

  @override
  List<Object?> get props => [user];
}
