part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object?> get props => [];
}

class AuthInitialState extends AuthState {}

class AuthLoadingState extends AuthState {}

class AuthLoggedInState extends AuthState {}

class AuthLoggedOutState extends AuthState {}

class AuthLoginState extends AuthState {}

class AuthSuccesState extends AuthState {}

class AuthErrorState extends AuthState {
  final String? errorMessage;

  const AuthErrorState({
    this.errorMessage,
  });

  @override
  List<Object?> get props => [errorMessage];
}
