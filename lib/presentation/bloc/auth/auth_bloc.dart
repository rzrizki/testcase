import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/core/error/failure.dart';
import 'package:majootestcase/core/utils/constant.dart';
import 'package:majootestcase/core/utils/shared_preferences_manager.dart';
import 'package:majootestcase/domain/entities/user/user.dart';
import 'package:majootestcase/domain/use_cases/login.dart';

part 'auth_state.dart';
part 'auth_event.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final Login login;
  final SharedPreferencesManager sharedPreferencesManager;

  AuthBloc({
    required this.login,
    required this.sharedPreferencesManager,
  }) : super(AuthInitialState()) {
    on<LoadAuthStatusEvent>(_onLoadAuthStatusEvent);
    on<SubmitLogOutEvent>(_onSubmitLogOutEvent);
    on<SubmitLoginEvent>(_onSubmitLoginEvent, transformer: restartable());
  }

  void _onLoadAuthStatusEvent(
    LoadAuthStatusEvent event,
    Emitter<AuthState> emit,
  ) async {
    final isLoggedIn =
        sharedPreferencesManager.getBool(PreferenceKey.isLoggedIn) ?? false;
    if (isLoggedIn) {
      emit(AuthLoggedInState());
    } else {
      emit(AuthLoginState());
    }
  }

  void _onSubmitLogOutEvent(
    SubmitLogOutEvent event,
    Emitter<AuthState> emit,
  ) async {
    final clearSuccess = await sharedPreferencesManager.clearAll();
    if (clearSuccess) {
      emit(AuthLoggedOutState());
    } else {
      emit(const AuthErrorState(errorMessage: ConstantMessage.unknownError));
    }
  }

  void _onSubmitLoginEvent(
    SubmitLoginEvent event,
    Emitter<AuthState> emit,
  ) async {
    emit(AuthLoadingState());

    final resultLogin = await login(
      ParamsLogin(user: event.user),
    );

    emit(
      await resultLogin.fold(
        (failure) {
          var errorMessage = ConstantMessage.unknownError;
          if (failure is DatabaseFailure) {
            errorMessage = failure.errorMessage;
          }
          return AuthErrorState(errorMessage: errorMessage);
        },
        (response) async {
          await sharedPreferencesManager.putBool(
            PreferenceKey.isLoggedIn,
            true,
          );
          await sharedPreferencesManager.putString(
            PreferenceKey.email,
            response.email ?? '',
          );
          await sharedPreferencesManager.putString(
            PreferenceKey.userName,
            response.userName ?? '',
          );
          return AuthSuccesState();
        },
      ),
    );
  }
}
