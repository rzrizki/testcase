import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final Widget? retryButton;

  const ErrorScreen({
    Key? key,
    this.retryButton,
    this.message = "",
    this.retry,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(Icons.error, size: 64),
          const SizedBox(height: 12),
          Text(
            message,
            style: TextStyle(fontSize: 12, color: textColor ?? Colors.black),
          ),
          retry != null
              ? Column(
                  children: [
                    const SizedBox(
                      height: 8,
                    ),
                    retryButton ??
                        IconButton(
                          onPressed: () {
                            if (retry != null) {
                              retry!();
                            }
                          },
                          icon: const Icon(Icons.refresh_sharp),
                        ),
                  ],
                )
              : Container()
        ],
      ),
    );
  }
}
