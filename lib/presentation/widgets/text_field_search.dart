import 'dart:async';

import 'package:flutter/material.dart';

class TextFieldSearch extends StatefulWidget {
  final Function(String keyword) onSearchResult;
  final InputDecoration? decoration;
  final bool isShowClearIcon;
  final bool autoFocus;

  const TextFieldSearch(
    this.onSearchResult, {
    this.decoration,
    this.isShowClearIcon = true,
    this.autoFocus = false,
    Key? key,
  }) : super(key: key);

  @override
  _TextFieldSearchState createState() => _TextFieldSearchState();
}

class _TextFieldSearchState extends State<TextFieldSearch> {
  final controllerSearch = TextEditingController();
  final _valueNotifierSearch = ValueNotifier('');
  final _focusNodeTextField = FocusNode();

  var _isFirstTime = true;
  Timer? _debounce;
  String _keyword = '';

  @override
  void initState() {
    controllerSearch.addListener(_onSearching);
    super.initState();
  }

  @override
  void dispose() {
    controllerSearch.removeListener(_onSearching);
    controllerSearch.dispose();
    _debounce?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TextFormField(
            controller: controllerSearch,
            decoration: widget.decoration,
            onChanged: (value) => _valueNotifierSearch.value = value,
            focusNode: _focusNodeTextField,
            autofocus: widget.autoFocus,
          ),
        ),
        widget.isShowClearIcon
            ? ValueListenableBuilder(
                valueListenable: _valueNotifierSearch,
                builder: (BuildContext context, String value, Widget? child) {
                  return value.isEmpty
                      ? Container()
                      : GestureDetector(
                          onTap: () {
                            controllerSearch.clear();
                            _keyword = '';
                            _valueNotifierSearch.value = _keyword;
                            widget.onSearchResult.call(_keyword);
                            _focusNodeTextField.requestFocus();
                          },
                          child: const Padding(
                            padding: EdgeInsets.only(left: 8.0),
                            child: Icon(
                              Icons.clear,
                              size: 20,
                              color: Colors.grey,
                            ),
                          ),
                        );
                },
              )
            : Container(),
      ],
    );
  }

  void _onSearching() {
    var keyword = controllerSearch.text.trim();
    if (_isFirstTime && keyword.isEmpty) {
      return;
    }
    _isFirstTime = false;
    if (_debounce?.isActive ?? false) {
      _debounce?.cancel();
    }
    _debounce = Timer(const Duration(milliseconds: 600), () {
      if (keyword == _keyword) {
        return;
      }
      _keyword = keyword;
      widget.onSearchResult.call(_keyword);
    });
  }
}
