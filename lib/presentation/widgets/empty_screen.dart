import 'package:flutter/material.dart';

class EmptyScreen extends StatelessWidget {
  final String message;
  final Color? textColor;

  const EmptyScreen({
    Key? key,
    this.message = "",
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(Icons.search, size: 64),
          const SizedBox(height: 12),
          Text(
            message,
            style: TextStyle(fontSize: 12, color: textColor ?? Colors.black),
          ),
        ],
      ),
    );
  }
}
