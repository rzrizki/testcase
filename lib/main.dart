import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:majootestcase/app.dart';
import 'package:majootestcase/injection_container.dart' as di;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  di.sl.allowReassignment = true;
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) => runApp(const MyApp()));
}
