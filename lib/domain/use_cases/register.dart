import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/core/error/failure.dart';
import 'package:majootestcase/core/use_cases/use_case.dart';
import 'package:majootestcase/domain/entities/user/user.dart';
import 'package:majootestcase/domain/repositories/user_repository.dart';

class Register implements UseCase<String, ParamsRegister> {
  final UserRepository userRepository;

  Register({required this.userRepository});

  @override
  Future<Either<Failure, String>> call(ParamsRegister params) async {
    return await userRepository.register(params.user);
  }
}

class ParamsRegister extends Equatable {
  final User user;

  const ParamsRegister({
    required this.user,
  });

  @override
  List<Object?> get props => [user];
}
