import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/core/error/failure.dart';
import 'package:majootestcase/core/use_cases/use_case.dart';
import 'package:majootestcase/domain/entities/user/user.dart';
import 'package:majootestcase/domain/repositories/user_repository.dart';

class Login implements UseCase<User, ParamsLogin> {
  final UserRepository userRepository;

  Login({required this.userRepository});

  @override
  Future<Either<Failure, User>> call(ParamsLogin params) async {
    return await userRepository.login(params.user);
  }
}

class ParamsLogin extends Equatable {
  final User user;

  const ParamsLogin({
    required this.user,
  });

  @override
  List<Object?> get props => [user];
}
