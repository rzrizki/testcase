import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/core/error/failure.dart';
import 'package:majootestcase/core/use_cases/use_case.dart';
import 'package:majootestcase/domain/entities/movie/movie.dart';
import 'package:majootestcase/domain/repositories/movie_repository.dart';

class GetMovieList implements UseCase<List<Movie>, ParamsGetMovieList> {
  final MovieRepository movieRepository;

  GetMovieList({required this.movieRepository});

  @override
  Future<Either<Failure, List<Movie>>> call(ParamsGetMovieList params) async {
    return await movieRepository.getMovieList(params.query ?? '');
  }
}

class ParamsGetMovieList extends Equatable {
  final String? query;

  const ParamsGetMovieList({
    this.query,
  });

  @override
  List<Object?> get props => [query];
}
