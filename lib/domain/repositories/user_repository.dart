import 'package:dartz/dartz.dart';
import 'package:majootestcase/core/error/failure.dart';
import 'package:majootestcase/domain/entities/user/user.dart';

abstract class UserRepository {
  Future<Either<Failure, String>> register(User user);
  Future<Either<Failure, User>> login(User user);
}
