import 'package:dartz/dartz.dart';
import 'package:majootestcase/core/error/failure.dart';
import 'package:majootestcase/domain/entities/movie/movie.dart';

abstract class MovieRepository {
  Future<Either<Failure, List<Movie>>> getMovieList(String query);
}
