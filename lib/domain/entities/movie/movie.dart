import 'package:equatable/equatable.dart';
import 'package:majootestcase/domain/entities/movie/image.dart';
import 'package:majootestcase/domain/entities/movie/video.dart';

class Movie extends Equatable {
  final Image? image;
  final String? id;
  final String? title;
  final String? type;
  final int? rank;
  final String? star;
  final List<Video>? videoList;
  final int? year;
  final String? yearString;

  const Movie({
    this.image,
    this.id,
    this.title,
    this.type,
    this.rank,
    this.star,
    this.videoList,
    this.year,
    this.yearString,
  });

  @override
  List<Object?> get props => [
        image,
        id,
        title,
        type,
        rank,
        star,
        videoList,
        year,
        yearString,
      ];
}
