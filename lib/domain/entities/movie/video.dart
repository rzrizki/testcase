import 'package:equatable/equatable.dart';
import 'package:majootestcase/domain/entities/movie/image.dart';

class Video extends Equatable {
  final Image? image;
  final String? id;
  final String? title;
  final String? duration;

  const Video({this.image, this.id, this.title, this.duration});

  @override
  List<Object?> get props => [image, id, title, duration];
}
