import 'package:equatable/equatable.dart';

class Image extends Equatable {
  final int? height;
  final String? imageUrl;
  final int? width;

  const Image({
    this.height,
    this.imageUrl,
    this.width,
  });

  @override
  List<Object?> get props => [height, imageUrl, width];
}
