import 'package:equatable/equatable.dart';

class ApiDataFailure extends Equatable {
  final int? statusCode;
  final String? message;
  final String? httpMessage;
  final Map<String, dynamic>? errorData;

  const ApiDataFailure({
    this.statusCode,
    this.message,
    this.httpMessage,
    this.errorData,
  });

  @override
  List<Object?> get props => [
        statusCode,
        message,
        httpMessage,
        errorData,
      ];
}
