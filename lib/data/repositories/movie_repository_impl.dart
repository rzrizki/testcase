import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:majootestcase/core/error/failure.dart';
import 'package:majootestcase/core/network/network_info.dart';
import 'package:majootestcase/core/utils/constant.dart';
import 'package:majootestcase/core/utils/error_helper.dart';
import 'package:majootestcase/data/data_sources/remote/movie_remote_data_source.dart';
import 'package:majootestcase/domain/entities/api_data_failure/api_data_failure.dart';
import 'package:majootestcase/domain/entities/movie/movie.dart';
import 'package:majootestcase/domain/repositories/movie_repository.dart';

class MovieRepositoryImpl implements MovieRepository {
  final MovieRemoteDataSource movieRemoteDataSource;
  final NetworkInfo networkInfo;

  MovieRepositoryImpl({
    required this.movieRemoteDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<Movie>>> getMovieList(String query) async {
    var isConnected = await networkInfo.isConnected;
    if (isConnected) {
      try {
        final result = await movieRemoteDataSource.getMovieList(query);
        return Right(result.map((model) => model.toEntity()).toList());
      } on DioError catch (error) {
        if (error.response == null) {
          return Left(ServerFailure(ApiDataFailure(message: error.message)));
        }
        var errorMessage = ErrorHelper.getErrorMessageFromEndpoint(
          error.response?.data,
          error.message,
          error.response?.statusCode ?? 400,
        );
        return Left(ServerFailure(ApiDataFailure(
          message: errorMessage,
          statusCode: error.response?.statusCode,
          httpMessage: error.message,
        )));
      } on TypeError catch (error) {
        return Left(ParsingFailure(error.toString()));
      }
    } else {
      return Left(ConnectionFailure(ConstantMessage.connectionError));
    }
  }
}
