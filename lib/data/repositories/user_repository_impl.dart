import 'package:dartz/dartz.dart';
import 'package:majootestcase/core/error/exception.dart';
import 'package:majootestcase/core/error/failure.dart';
import 'package:majootestcase/core/utils/constant.dart';
import 'package:majootestcase/data/data_sources/local/user_local_data_soure.dart';
import 'package:majootestcase/data/models/user/user_model.dart';
import 'package:majootestcase/domain/entities/user/user.dart';
import 'package:majootestcase/domain/repositories/user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  final UserLocalDataSource userLocalDataSource;

  UserRepositoryImpl({
    required this.userLocalDataSource,
  });

  @override
  Future<Either<Failure, String>> register(User user) async {
    try {
      final userResult = await userLocalDataSource.getUserByEmail(
        UserModel.fromEntity(user),
        false,
      );
      if (userResult == null) {
        final result = await userLocalDataSource.insertUser(
          UserModel.fromEntity(user),
        );
        return Right(result);
      } else {
        return Left(DatabaseFailure(ConstantMessage.userAlreadyExisted));
      }
    } on DatabaseException catch (e) {
      return Left(DatabaseFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, User>> login(User user) async {
    try {
      final result = await userLocalDataSource.getUserByEmail(
        UserModel.fromEntity(user),
        true,
      );
      if (result != null) {
        return Right(result.toEntity());
      } else {
        return Left(DatabaseFailure(ConstantMessage.loginFailed));
      }
    } on DatabaseException catch (e) {
      return Left(DatabaseFailure(e.message));
    }
  }
}
