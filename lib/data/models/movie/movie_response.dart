import 'package:equatable/equatable.dart';

import 'movie_model.dart';

class MovieResponse extends Equatable {
  final List<MovieModel>? movieList;
  final String? query;
  final int? v;

  const MovieResponse({this.movieList, this.query, this.v});

  factory MovieResponse.fromJson(Map<String, dynamic>? json) => MovieResponse(
        movieList: (json?['d'] as List<dynamic>?)
            ?.map((e) => MovieModel.fromJson(e as Map<String, dynamic>))
            .toList(),
        query: json?['q'] as String?,
        v: json?['v'] as int?,
      );

  Map<String, dynamic> toJson() => {
        'd': movieList?.map((e) => e.toJson()).toList(),
        'q': query,
        'v': v,
      };

  @override
  List<Object?> get props => [movieList, query, v];
}
