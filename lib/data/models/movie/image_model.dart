import 'package:equatable/equatable.dart';
import 'package:majootestcase/domain/entities/movie/image.dart';

class ImageModel extends Equatable {
  final int? height;
  final String? imageUrl;
  final int? width;

  const ImageModel({this.height, this.imageUrl, this.width});

  factory ImageModel.fromJson(Map<String, dynamic> json) => ImageModel(
        height: json['height'] as int?,
        imageUrl: json['imageUrl'] as String?,
        width: json['width'] as int?,
      );

  Map<String, dynamic> toJson() => {
        'height': height,
        'imageUrl': imageUrl,
        'width': width,
      };

  Image toEntity() {
    return Image(
      height: height,
      imageUrl: imageUrl,
      width: width,
    );
  }

  @override
  List<Object?> get props => [height, imageUrl, width];
}
