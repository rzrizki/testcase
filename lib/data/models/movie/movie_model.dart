import 'package:equatable/equatable.dart';
import 'package:majootestcase/domain/entities/movie/movie.dart';

import 'image_model.dart';
import 'video_model.dart';

class MovieModel extends Equatable {
  final ImageModel? i;
  final String? id;
  final String? l;
  final String? q;
  final int? rank;
  final String? s;
  final List<VideoModel>? v;
  final int? vt;
  final int? y;
  final String? yr;

  const MovieModel({
    this.i,
    this.id,
    this.l,
    this.q,
    this.rank,
    this.s,
    this.v,
    this.vt,
    this.y,
    this.yr,
  });

  factory MovieModel.fromJson(Map<String, dynamic> json) => MovieModel(
        i: json['i'] == null
            ? null
            : ImageModel.fromJson(json['i'] as Map<String, dynamic>),
        id: json['id'] as String?,
        l: json['l'] as String?,
        q: json['q'] as String?,
        rank: json['rank'] as int?,
        s: json['s'] as String?,
        v: (json['v'] as List<dynamic>?)
            ?.map((e) => VideoModel.fromJson(e as Map<String, dynamic>))
            .toList(),
        vt: json['vt'] as int?,
        y: json['y'] as int?,
        yr: json['yr'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'i': i?.toJson(),
        'id': id,
        'l': l,
        'q': q,
        'rank': rank,
        's': s,
        'v': v?.map((e) => e.toJson()).toList(),
        'vt': vt,
        'y': y,
        'yr': yr,
      };

  Movie toEntity() {
    return Movie(
      id: id,
      image: i?.toEntity(),
      type: q,
      videoList: v?.map((e) => e.toEntity()).toList(),
      year: y,
      yearString: yr,
      title: l,
      star: s,
      rank: rank,
    );
  }

  @override
  List<Object?> get props => [i, id, l, q, rank, s, v, vt, y, yr];
}
