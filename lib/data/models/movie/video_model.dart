import 'package:equatable/equatable.dart';
import 'package:majootestcase/domain/entities/movie/video.dart';

import 'image_model.dart';

class VideoModel extends Equatable {
  final ImageModel? i;
  final String? id;
  final String? l;
  final String? s;

  const VideoModel({this.i, this.id, this.l, this.s});

  factory VideoModel.fromJson(Map<String, dynamic> json) => VideoModel(
        i: json['i'] == null
            ? null
            : ImageModel.fromJson(json['i'] as Map<String, dynamic>),
        id: json['id'] as String?,
        l: json['l'] as String?,
        s: json['s'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'i': i?.toJson(),
        'id': id,
        'l': l,
        's': s,
      };

  Video toEntity() {
    return Video(
      image: i?.toEntity(),
      id: id,
      title: l,
      duration: s,
    );
  }

  @override
  List<Object?> get props => [i, id, l, s];
}
