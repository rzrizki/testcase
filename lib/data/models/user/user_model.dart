import 'package:equatable/equatable.dart';
import 'package:majootestcase/domain/entities/user/user.dart';

class UserModel extends Equatable {
  final String? email;
  final String? password;
  final String? userName;

  const UserModel({
    required this.email,
    required this.password,
    required this.userName,
  });

  factory UserModel.fromEntity(User user) => UserModel(
        email: user.email,
        password: user.password,
        userName: user.userName,
      );

  User toEntity() {
    return User(
      email: email,
      password: password,
      userName: userName,
    );
  }

  UserModel.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() => {
        'email': email,
        'password': password,
        'username': userName,
      };

  @override
  List<Object?> get props => [email, password, userName];
}
