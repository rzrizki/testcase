import 'package:majootestcase/core/error/exception.dart';
import 'package:majootestcase/core/utils/constant.dart';
import 'package:majootestcase/data/data_sources/db/database_helper.dart';
import 'package:majootestcase/data/models/user/user_model.dart';

abstract class UserLocalDataSource {
  Future<String> insertUser(UserModel user);
  Future<UserModel?> getUserByEmail(UserModel user, bool usePassword);
}

class UserLocalDataSourceImpl implements UserLocalDataSource {
  final DatabaseHelper databaseHelper;

  UserLocalDataSourceImpl({required this.databaseHelper});

  @override
  Future<String> insertUser(UserModel user) async {
    try {
      await databaseHelper.insertData(DatabaseTable.user, user.toJson());
      return ConstantMessage.insertUserSuccess;
    } catch (e) {
      throw DatabaseException(e.toString());
    }
  }

  @override
  Future<UserModel?> getUserByEmail(UserModel user, bool usePassword) async {
    String where = '${DatabaseTable.columnEmail} = ?';
    List<Object> whereArgs = [user.email ?? ''];

    if (usePassword) {
      where += ' and ${DatabaseTable.columnPassword} = ?';
      whereArgs.add(user.password ?? '');
    }

    final result = await databaseHelper.getDataByQuery(
      DatabaseTable.user,
      where: where,
      whereArgs: whereArgs,
    );
    if (result != null) {
      return UserModel.fromJson(result);
    } else {
      return null;
    }
  }
}
