import 'dart:async';

import 'package:majootestcase/core/utils/constant.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static DatabaseHelper? _databaseHelper;
  DatabaseHelper._instance() {
    _databaseHelper = this;
  }

  factory DatabaseHelper() => _databaseHelper ?? DatabaseHelper._instance();

  static Database? _database;

  Future<Database?> get database async {
    _database ??= await _initDb();
    return _database;
  }

  Future<Database> _initDb() async {
    final path = await getDatabasesPath();
    final databasePath = '$path/majoomovie.db';

    var db = await openDatabase(databasePath, version: 1, onCreate: _onCreate);
    return db;
  }

  void _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE  ${DatabaseTable.user} (
        id INTEGER PRIMARY KEY,
        ${DatabaseTable.columnEmail} TEXT,
        ${DatabaseTable.columnPassword} TEXT,
        ${DatabaseTable.columnUserName} TEXT
      );
    ''');
  }

  Future<int> insertData(String table, Map<String, dynamic> value) async {
    final db = await database;
    return await db!.insert(table, value);
  }

  Future<Map<String, dynamic>?> getDataByQuery(
    String table, {
    required String where,
    required List<Object> whereArgs,
  }) async {
    final db = await database;
    final results = await db!.query(
      table,
      where: where,
      whereArgs: whereArgs,
    );

    if (results.isNotEmpty) {
      return results.first;
    } else {
      return null;
    }
  }
}
