import 'package:dio/dio.dart';
import 'package:majootestcase/data/models/movie/movie_model.dart';
import 'package:majootestcase/data/models/movie/movie_response.dart';

abstract class MovieRemoteDataSource {
  /// Panggil endpoint [ApiConfig.baseUrl]/auto-complete
  ///
  /// Throws [DioError] untuk semua error kode
  Future<List<MovieModel>> getMovieList(String query);
}

class MovieRemoteDataSourceImpl implements MovieRemoteDataSource {
  final Dio dio;

  MovieRemoteDataSourceImpl({required this.dio});

  @override
  Future<List<MovieModel>> getMovieList(String query) async {
    const path = 'auto-complete';
    final response = await dio.get(
      path,
      queryParameters: {
        'q': query,
      },
    );
    if (response.statusCode == 200) {
      return MovieResponse.fromJson(response.data).movieList ?? [];
    } else {
      throw DioError(requestOptions: RequestOptions(path: path));
    }
  }
}
